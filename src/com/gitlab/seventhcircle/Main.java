package com.gitlab.seventhcircle;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("Enter a the number of pi you'd like to see!");

            int input = System.in.read() - 48;
            double pi = Math.PI;
            if (input > 17) return;

            String number = String.valueOf(pi);
            String[] digits = number.split("(?<=.)");

            System.out.println(digits[input]);

        } catch (IOException ioexception) {
            System.out.println("IO ERROR| " + ioexception);
        }
    }
}
